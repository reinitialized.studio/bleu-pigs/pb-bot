--  src/pigBot.lua
--[[
    pigBot.lua 1.0:1618
    https://gitlab.com/bleu-pigs/pig-bot
    The official Bleu Pigs bot
]]
------  PRE-INITIALIZATION
----   Localization, Variables, and Constants
local ENVIRONMENT = getfenv()
local FILE_PATH = "src/Modules"
local passedArguments = rawget(
  _G,
  "args"
) or {}
----  preload required libraries
local Discordia = require("discordia")
local LuaSignal = require("../".. FILE_PATH .."/LuaSignal.lua")
local FileSystem = require("coro-fs")
local sleep = require("timer").sleep
------  MANAGED MODULES
--[[
  Readies all components into virtual wrappers
  
  - ensures each module has its own dedicated environment
  - ensures each module contains info on itself (name, path to file, etc)
  - special "System" module for determining deployment and other critical info
]]
local ManagedModules = {
  loaded = {}
} do
  ----  PRE-SETUP
  local ENVIRONMENT = getfenv()
  local PATH_TO_MODULES = "src/Modules/"
  local PASSED_ARGUMENTS = rawget(
    ENVIRONMENT,
    "args"
  )
  local loadedModules = ManagedModules.loaded
  local lookupModuleByThread = setmetatable(
    {},
    {
      __mode = "k"
    }
  )
  --  "System" module and PigBot container root
  --  The "root" module which all other modules will base their environment off of.
  --  "System" will contain important information about PigBot and how it was deployed
  local baseModule, baseAPI = {
    statistics = {
      threads = setmetatable(
        {},
        {
          __mode = "v"
        }
      )
    },
    name = "System",
    path = "./",
    deployment = PASSED_ARGUMENTS[2], -- first arg is passed init file (aka pigBot.lua) to Luvit
    branch = PASSED_ARGUMENTS[3],
    commit = PASSED_ARGUMENTS[4]
  }, {}
  --  localize environment variables which could be looked up after the fact.
  --  This will prevent recursive lockups
  local coroutine = ENVIRONMENT.coroutine
  local error = ENVIRONMENT.error
  local type = ENVIRONMENT.type
  local string = ENVIRONMENT.string
  --  BaseEnvironment for System. All modules readied will inherit their environment
  --  from BaseEnvironment
  local baseEnvironment = setmetatable(
    {
      ----  expose libraries and other variables
      Discordia = Discordia,
      FileSystem = FileSystem,
      sleep = sleep,
      -- other stuff
      module = baseModule,
      coroutine = {
        create = function(providedFunction)
          if Debugging ~= nil then
            Debugging.checkType(
              providedFunction,
              "coroutine",
              1
            )
          else
            local __type_providedFunction = type(providedFunction)
            if __type_providedFunction ~= "function" then
              error(
                string.format(
                  "bad argument #1 (expected function, got %s)",
                  __type_providedFunction
                ),
                2
              )
            end
          end
          ----  main function logic
          local newThread = coroutine.create(providedFunction)
          local callingModule = lookupModuleByThread[coroutine.running()]
          if callingModule ~= nil then
            callingModule.statistics.threads[#callingModule.statistics.threads + 1] = newThread
          end
          return newThread
        end,
        wrap = function(providedFunction)
          if Debugging ~= nil then
            Debugging.checkType(
              providedFunction,
              "coroutine",
              1
            )
          else
            local __type_providedFunction = type(providedFunction)
            if __type_providedFunction ~= "function" then
              error(
                string.format(
                  "bad argument #1 (expected function, got %s)",
                  __type_providedFunction
                ),
                2
              )
            end
          end
          ----  main function logic
          local newThread = coroutine.create(providedFunction)
          local callingModule = lookupModuleByThread[coroutine.running()]
          if callingModule ~= nil then
            callingModule.statistics.threads[#callingModule.statistics.threads + 1] = newThread
          end
          return function(...)
            return coroutine.resume(
              newThread,
              ...
            )
          end
        end,
        resume = coroutine.resume,
        running = coroutine.running,
        status = coroutine.status,
        yield = coroutine.yield
      }
    },
    {
      __index = function(self, index)
        if ENVIRONMENT[index] == nil then
          return PigBot[index]
        end
        return ENVIRONMENT[index]
      end,
      __metatable = "BaseEnvironment::".. baseModule.name
    }
  )
  --  override default environment to BaseEnvironment
  setfenv(
    0,
    baseEnvironment
  )
  setfenv(
    1,
    baseEnvironment
  )
  --  manually link "System" to ManagedModules
  loadedModules[baseModule.name] = baseModule
  lookupModuleByThread[coroutine.running()] = baseModule
  baseAPI[baseModule.name] = baseModule
  _G.PigBot = baseAPI   -- reroute baseAPI as PigBot root
  require("BotConfig")
  require("AuthenticationSettings")
  ----  API INITIALIZATION
  --  Object  readySource(string pathToSource, string name, boolean initializeNow)
  --    Using pathToSource, will compile and wrap the source into a
  --    coroutine, being interfaced via the Module Object. If initializeNow
  --    is true, will load the coroutine immediately.
  function readySource(pathToSource, name, initializeNow)
    --  debug logic
    if Debugging ~= nil then
      Debugging.checkType(
        pathToSource,
        "string",
        1
      )
      Debugging.checkType(
        name,
        "string",
        2
      )
      Debugging.checkType(
        initializeNow,
        {
          "boolean",
          "nil"
        },
        3
      )
    else
      --  Debugging module isn't initialized yet, do manual logic
      local __type_pathToSource = type(pathToSource)
      local __type_name = type(name)
      local __type_initializeNow = type(initializeNow)
      if __type_pathToSource ~= "string" then
        error("bad argument #1 (expected string, got ".. __type_pathToSource ..")", 2)
      end
      if __type_name ~= "string" then
        error("bad argument #2 (expected string, got ".. __type_name ..")", 2)
      end
      if __type_initializeNow ~= "boolean" then
        if __type_initializeNow == "nil" then
          initializeNow = false
        else
          error("bad argument #3 (expected boolean, got ".. __type_initializeNow ..")", 2)
        end
      end
    end
    ----  main function logic
    --  first, check to see if pathToSource is valid
    local source, failureReason = FileSystem.readFile(
      pathToSource .."/".. name,
      "0666"
    )
    if source == nil then
      if Debugging ~= nil then
        Debugging.terminate(
          "Unable to ready Module: %s is not a valid source path\nfailure reason: %s",
          pathToSource .."/".. name,
          failureReason
        )
      else
        error("Unable to ready Module: ".. pathToSource .."/".. name .." is not a valid source path\nfailure reason: ".. failureReason, 2)
      end
    end
    --  second, compile the source and perform error handling if needed
    local sourceCompiled, compileFailure = loadstring(source)
    if sourceCompiled == nil then
      if Debugging ~= nil then
        Debugging.terminate(
          "Unable to ready Module %s: source compile failure captured\n%s",
          name,
          compileFailure
        )
      else
        error(
          string.format(
            "Unable to ready Module %s: source compile failure caught\n%s",
            name,
            compileFailure
          ),
          2
        )
      end
    end
    --  third, initialize the Module Object and its internal properties
    name = name:gsub(
      ".lua",
      ""
    )
    local ModuleWrapper, moduleApi = {
      statistics = {
        threads = setmetatable(
          {},
          {
            __mode = "v"
          }
        )
      },
      name = name,
      path = pathToSource .."/".. name
    }, {}
    local moduleReadySignal = LuaSignal.new()
    local statistics = {
      threads = setmetatable(
        {},
        {
          __mode = "v"
        }
      )
    }
    local environment
    environment = setmetatable(
      {
        root = moduleApi,
        module = ModuleWrapper,
        ready = function(...)
          getfenv(2).ready = nil
          if Debugging ~= nil then
            Debugging.write(
              "Module %s initialized",
              name
            )
          else
            if BotConfig.Debugging.Enabled == true then
              print(
                string.format(
                  "Module %s is initialized",
                  name
                )
              )
            end
          end
          moduleReadySignal:fire(...)
        end,
        PigBot = baseAPI
      },
      {
        __index = baseEnvironment,
        __newindex = function(self, key, value)
          if key:sub(0, 9) == "declare__" then
            key = key:sub(10)
            --  debug logic
            local type = type(value)
            if Debugging ~= nil then
              Debugging.write(
                "Declaring %s-%s to module %s",
                type,
                key,
                name
              )
            else
              if BotConfig.Debugging.Enabled == true then
                print(
                  string.format(
                    "Declaring %s-%s to module %s",
                    type,
                    key,
                    name
                  )
                )
              end
            end
            --  set new entry
            moduleApi[key] = value
          else
            rawset(
              environment,
              key,
              value
            )
          end
        end,
        __metatable = "BaseEnvironment for ".. name
      }
    )
    setfenv(
      sourceCompiled,
      environment
    )
    local luaThread = coroutine.create(sourceCompiled)
    lookupModuleByThread[luaThread] = moduleWrapper
    ----  wrapper api
    function ModuleWrapper:run(...)
      local threadRan, failureResponse = coroutine.resume(
        luaThread,
        ...
      )
      if threadRan == false then
        if Debugging ~= nil then
          Debugging.terminate(
            "Unable to run Module %s: runtime error caught\n%s",
            name,
            failureResponse
          )
        else
          error(
            string.format(
              "Unable to run Module %s: runtime error caught:\n%s",
              name,
              failureResponse
            ),
            2
          )
        end
      end
      if environment.ready ~= nil then
        moduleReadySignal.Event:wait()
      end
      moduleReadySignal:destroy()
      baseAPI[name] = moduleApi
    end
    ----  finish up
    ModuleWrapper.environment = environment
    loadedModules[name] = ModuleWrapper
    if initializeNow == true then
      return ModuleWrapper:run()
    end
    return ModuleWrapper
  end
  --  Object  ManagedModules:getByThread(coroutine thread)
  --    Returns the module associated with thread, if any.
  function ManagedModules:getByThread(thread)
    if Debugging ~= nil then
      Debugging.checkType(
        thread,
        {
          "coroutine",
          "nil"
        },
        1
      )
    else
      local __type_thread = type(thread)
      if __type_thread ~= "coroutine" then
        if __type_thread == "nil" then
          thread = coroutine.running()
        else
          error(
            string.format(
              "bad argument #1 (expected thread, got %s)",
              __type_thread
            ),
            2
          )
        end
      end
    end
    ----  main function logic
    if Debugging ~= nil then
      return Debugging.assert(
        getModuleByThread[thread],
        "cannot continue with execution: thread is not linked to module, in which this API call is dependent on"
      )
    else
      return assert(
        getModuleByThread[thread],
        "cannot continue with execution: thread is not linked to module, in which this API call is dependent on"
      )
    end
    return getModuleByThread[thread]  
  end
end
------  PigBot Initialization Process
------    PigBot is now deployed via GitLab. Utilize the variables provided to load
------    according to our type of deployment
----  PRE-INITIALIZATION
local SIGNAL_pbReady = LuaSignal.new()
local OBJECT_pbClient = Discordia.Client(
  {
    cacheAllMembers = true,
    syncGuilds = true
  }
)


-- local pbReady = LuaSignal.new()
-- PigBot.pbReady = pbReady.Event
-- local pbClient = Discordia.Client(
--   {
--     cacheAllMembers = true,
--     syncGuilds = true
--   }
-- ) PigBot.Client = pbClient
-- coroutine.wrap(
--   function()
--     ----  pbCore initialization
--     --  preload all Modules into ready
--     for entry in assert(FileSystem.scandir(FILE_PATH)) do
--       readySource(
--         FILE_PATH,
--         entry.name
--       )
--     end
--     --  Initialize core modules
--     ManagedModules.loaded["Logging"]:run()
--     ManagedModules.loaded["Debugging"]:run()
--     ManagedModules.loaded["Utilities"]:run()
--     ManagedModules.loaded["Scheduler"]:run()
--     ManagedModules.loaded["WebServer"]:run()
--     --  setup ready listener
--     Client:on(
--       "ready",
--       function()
--         --  finalize module initialization
--         ManagedModules.loaded["DiscordExtensions"]:run()
--         ManagedModules.loaded["UserData"]:run()
--         ManagedModules.loaded["NewMember"]:run()
--         --  fire Ready
--         pbReady:fire()
--         pbReady:destroy()
--         pbReady = nil
--         PigBot.pbReady = nil
--       end
--     )
--     if System.deployment == "test" then
--       ----  PigBot is being deployed via a CIPipeline test!
--       --  


--       -- --  TEST 1: perform API tests
--       -- readySource(
--       --   "../src/tests/", 
--       --   "BattleTester.lua", 
--       --   true
--       -- )
--       -- Client:run("Bot ".. BotConfig.Authentication.PIGBOTSERVICES_TOKEN)
--       -- pbReady.Event:wait()
--       -- for entry in assert(FileSystem.scandir("src/tests/modules")) do
--       --   require("../src/tests/modules/".. entry.name)
--       -- end
--       -- --  tests complete, exit cleanly.
--       -- Client:stop()
--       -- os.exit()
--     elseif System.deployment:sub(0, 7) == "deploy-" then
--       ----  PigBot is being deployed! Prepare for initialization
--       --  configuration is designed to load based on deployment, automagically
--       --  migrating this key-point to the staging/production server as instructed
--       pbClient:run("Bot ".. BotConfig.Authentication.PIGBOTSERVICES_TOKEN)
--     end
--   end
-- )()