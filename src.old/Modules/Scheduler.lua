--  Modules/Scheduler.lua
--[[
    PigBot Scheduler
    Based off https://gist.github.com/Deco/1818054
    Modified to suite needs of PigBot
]]
------  INITIALIZATION
----  variables
local threads = {}
local threadsLookup = setmetatable(
  {},
  {
    __mode = "k"
  }
)
----  constants
local STATUS = "%s suspended"
------  API
--  void  Scheduler.wait(number time)
--    Suspends the running thread for time seconds
function declare__wait(time)
  Debugging.checkType(
    time,
    "number",
    1
  )
  --  logic
  threads[#threads + 1] = {
    coroutine.running(),
    os.time() + time,
    debug.traceback()
  }
  return coroutine.yield()
end
--  void  Scheduler.spawn(function func)
--    Wraps func in a new thread, and inserts it to be ran asap
function declare__spawn(func, ...)
  Debugging.checkType(
    func,
    "function",
    2
  )
  --  logic
  threads[#threads + 1] = {
    coroutine.create(func),
    os.time(),
    debug.traceback(),
    {...}
  }
end
--  void  Scheduler.delay(number time, function func, ...)
function declare__delay(time, func, ...)
  Debugging.checkType(
    time,
    "number",
    1
  )
  Debugging.checkType(
    func,
    "function",
      2
  )
  --  logic
  threads[#threads + 1] = {
    coroutine.create(func),
    os.time() + time,
    debug.traceback(),
    {...}
  }
end
--  void  sortTasks(a, b)
--    Logic for the table.sort call
local function sortTasks(a, b)
  Debugging.checkType(
    a,
    "table",
    1
  )
  Debugging.checkType(
    b,
    "table",
    2
  )
  return a[2] < b[2]
end
------  FINALIZE MODULE
--  add our variables to global since theyre kinda important
_G.delay = root.delay
_G.spawn = root.spawn
_G.wait = root.wait
ready()
----  temporary status indictator. will eventually implement api for new system
local isIdle = false
root.spawn(
  function()
    while pbReady == nil do
      sleep(150)
    end
    pbReady:wait()
    --  logic
    while true do
      if #threads < 2 and isIdle == false then
        --  bot is idling
        Client:setStatus("idle")
        isIdle = true
      elseif #threads > 1 and isIdle == true then
        --  bot is active
        Client:setStatus("online")
        isIdle = false
      end
      Debugging.write(
        "PigBot is running fine!\nHandling %s unique tasks",
        #threads
      )
      Client:setGame(STATUS:format(#threads ..""))
      -- Scheduler.wait(15)
      sleep(15000)
    end
  end
)
--  scheduler
local thread, now
while true do
  now = os.time()
  if #threads > 0 then
    --  remove any dead threads
    for i = 1, #threads do
      if coroutine.status(threads[i][1]) == "dead" then
        Debugging.write(
          "%s is dead, removing",
          tostring(threads[i][1])
        )
        table.remove(threads, i)
      end
    end
    --  sort the threads
    table.sort(
      threads,
      sortTasks
    )
    if threads[1][2] <= now then
      thread = table.remove(
        threads,
        1
      )
      Debugging.write(
        "resuming %s\n%s",
        tostring(thread[1]),
        thread[3]
      )
      -- --  perform debug checks
      -- Debugging.checkType(
      --   thread,
      --   "table"
      -- )
      -- Debugging.checkType(
      --   thread[1],
      --   "thread"
      -- )
      -- Debugging.checkType(
      --   thread[2],
      --   "number"
      -- )
      -- Debugging.checkType(
      --   thread[3],
      --   "string"
      -- )
      --  logic
      local ran, failure = coroutine.resume(
        thread[1],
        now - thread[2],
        now,
        thread[4]
      )
      thread = nil
      if ran == false then
        Debugging.terminate(failure)
      end
    end
    sleep(500)
  else
    sleep(1500)
  end
end
