--  Modules/Logging.lua
------  INITIALIZATION
----   Localization, Variables, and Constants
------  MODULE API
----  CClass  RecordedEntry
----    Auto fills and keeps track of recorded data
local RECORDED_ENTRY = {}
--  table SEVERITY
--    ENUMS for representing the severity of a logged entry
local SEVERITY = {
  DEBUG = 0,
  INFO = 1,
  WARN = 2,
  ERROR = 3,
  FATAL = 4
}
local SEVERITY_LOOKUP = {}
for key, value in next, SEVERITY do
  SEVERITY_LOOKUP[value] = key
end
--  LuaSignal  ENTRY_CREATED_SIGNAL
--    LuaSignal which is fired when a new entry is recorded
local ENTRY_CREATED_SIGNAL = LuaSignal.new()
--  table  RECORDED_ENTRIES
--    Contains up to 200 recorded entries for the session
local RECORDED_ENTRIES = setmetatable(
  {},
  {
    __newindex = function(self, index, entry)
      if #self > 199 then
        rawset(
          self,
          index,
          nil
        )
      end
      rawset(
        self,
        #self + 1,
        entry
      )
    end
  }
)
--  RecordedEntry  RECORDED_ENTRY.new(string contents, number severity, boolean includeTraceback = false)
--    Creates a new RecordedEntry, representing a logged entry
function RECORDED_ENTRY.new(contents, severity, includeTraceback, ...)
  ----  debug logic
  local __type_contents = type(contents)
  local __type_severity = type(severity)
  local __type_includeTraceback = type(includeTraceback)
  assert(
    __type_contents == "string",
    "bad argument #1 (string expected, got ".. __type_contents ..")"
  )
  assert(
    __type_severity == "number",
    "bad argument #2 (number expected, got ".. __type_severity ..")"
  )
  if __type_includeTraceback == "nil" then
    includeTraceback = PigBot.BotConfig.Debugging.Enabled
    __type_includeTraceback = "boolean"
  end
  assert(
    __type_includeTraceback == "boolean",
    "bad argument #3 (boolean expected, got ".. __type_includeTraceback ..")"
  )
  --  typecheck complete, checking argument compatibility
  assert(
    SEVERITY_LOOKUP[severity] ~= nil,
    "bad argument #2 (invalid range, severity must be between 0 - 4)"
  )
  ----  main logic begin
  --  create the new entry
  local newEntry = {
    contents:format(...),
    severity,
    includeTraceback == true and debug.traceback() or "n/a",
    os.date(
      '%Y-%m-%d %H:%M:%S',
      os.time()
    ),
    coroutine.running(),
    getfenv(2)
  }
  --  insert into our table and fire our LuaSignal
  RECORDED_ENTRIES[#RECORDED_ENTRIES + 1] = newEntry
  ENTRY_CREATED_SIGNAL:fire(newEntry)
  return newEntry
end
----  Declaration of Logging
----  RecordedEntry  Logging.write(number severity, string contents)
----    Using RECORDED_ENTRY.new, creates a new RecordedEntry
function declare__write(severity, contents, ...)
  return RECORDED_ENTRY.new(
    contents,
    severity,
    nil,
    ...
  )
end
----  table  Logging.search(string pattern, table advancedSearch)
----    Searches for entries matching pattern. If advancedSearch exists, will
----    throw in more logic.
function declare__search(pattern, advancedSearch)
  local __type_pattern = type(pattern)
  local __type_advancedSearch = type(advancedSearch)
  assert(
    __type_pattern == "string",
    "bad argument #1 (string expected, got ".. __type_pattern ..")"
  )
  if __type_advancedSearch == "nil" then
    __type_advancedSearch = {}
  end
  assert(
    __type_advancedSearch == "table",
    "bad argument #2 (table expected, got ".. __type_advancedSearch ..")"
  )
  ----  main logic
  --  table  results
  --    Contains results of the search
  local results, resultBeingLookedAt = {}
  --  search loop
  for resultIndex = 1, #results do
    resultBeingLookedAt = results[resultIndex]
    --  actual matching logic
    if resultBeingLookedAt[1]:match(pattern) then
      --  pattern matches, add to results
      results[#results + 1] = result
    else
      --  TODO; implement advanced searching
    end
  end
  --  return our results
  return results
end
------  FINIALIZE
declare__EntryCreated = ENTRY_CREATED_SIGNAL.Event
----  send to output
local messageOut = "EntryCreated > [%s %s - %s@%s]\n%s\n\n%s\n\n"
ENTRY_CREATED_SIGNAL.Event:connect(
  function(entry)
    if entry[2] > 0 then
      local name = entry[6].module
      if name ~= nil then
        name = name.name
      else
        name = "GLOBAL"
      end
      print(
        messageOut:format(
          SEVERITY_LOOKUP[entry[2]],
          name,
          entry[5],
          entry[4],
          entry[1],
          entry[3]
        )
      )
    end
  end
)
----  fire ready
ready()
