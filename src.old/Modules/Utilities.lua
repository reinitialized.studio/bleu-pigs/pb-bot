-- Modules/Utilities.lua
------  INITIALIZATION
----  library loading/localization
declare__JSON = require("json")
local JSON = root.JSON
local httpRequest = require("coro-http").request
------  API DECLARATION
----  Counter  Utilities.newCounter(number timesTilTerminate)
----    Keeps track of how long a loop has been running.
function declare__newCounter(timesTilTerminate)
  ----  debug check
  Debugging.checkType(
    timesTilTerminate,
    "number",
    1
  )
  ----  function logic
  --  variables
  local timesTilTerminate = timesTilTerminate
  local counter = 0
  --  actual counter
  return function()
    if counter >= timesTilTerminate then
      return false
    end
    counter = counter + 1
    return true
  end
end
----  table  Utilities.sendHttpRequest(method, url, ...)
----    Sends an http request to url.
function declare__sendHttpRequest(method, url, ...)
    ------  debug check
  Debugging.checkType(
    method,
    "string",
    1
  )
  Debugging.checkType(
    url,
    "string",
    2
  )
  ------  function logic
  ----  variables
  local request = {
    method = method,
    url = url,
    headers = {}
  }
  --  check for headers
  local headers, __headers_type = select(
    1,
    ...
  )
  ---- variable check
  Debugging.checkType(
    headers,
    {
      "table",
      "nil"
    }
  )
  __headers_type = type(headers)
  if (__headers_type == "table") then
    request.headers = {}
  end
  ----  method check
  if method == "POST" then
    ---- variables
    local body, __body_type = headers ~= nil and select(
      2,
      ...
    ) or select(
      1,
      ...
    )
    ----  debug check
    Debugging.checkType(
      body,
      {
        "table",
        "string",
        "nil"
      }
    )
    ----  determine what to do with arguments
    __body_type = type(body)
    if (__body_type == "table") then
      request.body = JSON.encode(body)
    elseif (__body_type == "string") then
      request.body = body
    end
  end
  ----  perform request
  local counter = Utilities.newCounter(10)
  local success, headers, message = pcall(
    httpRequest,
    request.method,
    request.url,
    request.headers,
    request.body
  )
  counter()
  while success == false do
    --  check to see if we need to terminate
    if counter() == false then
      Debugging.write(
        "could not perform http request!\nurl: %s\nmethod: %s",
        url,
        method
      )
      Debugging.write(
        headers
      )
      return false
    end
    --  retry request
    success, headers, message = pcall(
      httpRequest,
      request.method,
      request.url,
      request.headers
    )
    --  did it succeed?
    if success then
      --  break the loop
      break
    end
    --  otherwise, wait
    wait(1)
  end
  --  attempt to JSON.decode response
  for i,v in ipairs(headers) do
    headers[v[1]] = v[2]
    headers[i] = nil
  end
  if headers["Content-Type"]:sub(0, 16) == "application/json" then
    message = JSON.decode(
      message,
      1,
      JSON.null
    )
  end
  --  return response
  return headers, message
end
------  FINALIZE
ready()
